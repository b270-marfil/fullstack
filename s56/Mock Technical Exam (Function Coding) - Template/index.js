function countLetter(letter, sentence) {
  let result = 0;

  /*
      1. 
          - Check first whether the letter is a single character.
          - If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
          - If letter is invalid, return undefined.
  */

  // Check first whether the letter is a single character.
  if (typeof letter === 'string' && letter.length === 1) {
    // Iterate over each character in the sentence and check if it matches the letter.
    for (let i = 0; i < sentence.length; i++) {
      if (sentence[i] === letter) {
        result++;
      }
    }
    return result;
  } else {
    return undefined; // Return undefined if the letter is not a single character.
  }
}

function isIsogram(text) {

    /*
        2. 
            - An isogram is a word where there are no repeating letters.
            - The function should disregard text casing before doing anything else.
            - If the function finds a repeating letter, return false. Otherwise, return true.
    */

  const letters = {};
    text = text.toLowerCase();

    for (let i = 0; i < text.length; i++) {
      const char = text.charAt(i);
      if (letters[char]) {
        return false;
      }
      letters[char] = true;
    }
    return true;
}

function purchase(age, price) {
    /*
        3. 
            - Return undefined for people aged below 13.
            - Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
            - Return the rounded off price for people aged 22 to 64.
            - The returned value should be a string.
    */

  // Return undefined for people aged below 13.
  if (age < 13) {
    return undefined;
  }

  // Return the discounted price for students aged 13 to 21 and senior citizens. (20% discount)
  if (age >= 13 && age <= 21 || age >= 65) {
    let discountedPrice = price * 0.8;
    return discountedPrice.toFixed(2);
  }

  // Return the original price for people aged 22 to 64.
  return price.toFixed(2);
}

function findHotCategories(items) {

    /*
        4. 
            Find categories that has no more stocks.
            The hot categories must be unique; no repeating categories.

            The passed items array from the test are the following:
            { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
            { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
            { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
            { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
            { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

            The expected output after processing the items array is ['toiletries', 'gadgets'].

            Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    */

    const hotCategories = new Set();

      for (const item of items) {
        if (item.stocks === 0) {
          hotCategories.add(item.category);
        }
      }

      return Array.from(hotCategories);
}

function findFlyingVoters(candidateA, candidateB) {
    /*
        5.  
            Find voters who voted for both candidate A and candidate B.

            The passed values from the test are the following:
            candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
            candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

            The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
            Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    */
    
  let flyingVoters = [];

  // Iterate over the candidateA array
  for (let i = 0; i < candidateA.length; i++) {
    // Check if the current ID in candidateA is present in candidateB
    if (candidateB.includes(candidateA[i])) {
      // If the ID is found in both arrays, add it to the flyingVoters array
      flyingVoters.push(candidateA[i]);
    }
  }

  // Return the array of flying voters
  return flyingVoters;
}


module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};