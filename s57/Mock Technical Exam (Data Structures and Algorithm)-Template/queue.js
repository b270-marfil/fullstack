let collection = [];

// Write the queue functions below.


function print() {
 if (isEmpty()) return ([]) 
   let str = '';
   for (let i = 0; i < collection.length; i++) {
     str += collection[i] + ', ';}
   console.log(str.trim());
}


function enqueue(element) {
      collection[collection.length] = element;
      return collection;
}


function dequeue() {
    if (isEmpty()) {
        return undefined;
      }
      let firstElement  = collection[collection.length - 1];
      for (let i = 0; i < collection.length - 1; i++) {
        collection[i] = collection[i + 1];
      }
      collection.length--;
      //return firstElement;
      let newfirstElement =[];
      newfirstElement[0]=collection[0];
      return newfirstElement;
}


function front() {
      if (isEmpty()) {
        return undefined;
      }
      return collection[0];
}

function size() {
      return collection.length;
}


function isEmpty() {
      return collection.length === 0;
}

module.exports = {
    print,
    enqueue,
    dequeue,
    size,
    front,
    isEmpty
};